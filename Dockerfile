FROM openjdk:16-alpine3.13

LABEL maintainer "bfistage"

HEALTHCHECK --interval=5s \
            --timeout=5s \
            CMD curl -f http://localhost:8000 || exit 1


EXPOSE 8000 
