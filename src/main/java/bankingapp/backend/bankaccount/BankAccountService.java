package bankingapp.backend.bankaccount;

import bankingapp.backend.bankaccount.agencies.AgencyEntity;
import bankingapp.backend.bankaccount.agencies.AgencyRepo;
import bankingapp.backend.bankaccount.bankaccounttypes.BankAccountTypeEntity;
import bankingapp.backend.bankaccount.bankaccounttypes.BankAccountTypeRepo;
import bankingapp.backend.bankaccount.banks.BankEntity;
import bankingapp.backend.bankaccount.banks.BankRepo;
import bankingapp.backend.user.UserEntity;
import bankingapp.backend.user.UserRepo;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BankAccountService {
    private final BankAccountRepo repo;
    private final UserRepo userRepo;
    private final BankAccountTypeRepo typeRepo;
    private final AgencyRepo agencyRepo;
    private final BankRepo bankRepo;

    public int count(UserEntity user) {
        List<BankAccountEntity> l = repo.findByUserOrderByBalanceDesc(user);
        return l.size();
    }

    @Transactional
    public String addAccount(String email, String type, String bankCode, String agencyCode) {
        if ((userRepo.findByEmail(email).isPresent())) {
            UserEntity user = userRepo.findByEmail(email).get();
            long code = (100000000 + (int) (Math.random() * ((999999999 - 100000000) + 1)));
            if (!repo.findByCode(code).isPresent()) {
                Optional<BankAccountTypeEntity> bAType = typeRepo.findById(type);
                Optional<BankEntity> bank = bankRepo.findBySwiftCode(bankCode);
                Optional<AgencyEntity> agency = agencyRepo.findByCode(agencyCode);
                if (bAType.isPresent()) {
                    if (bank.isPresent() && agency.isPresent()) {
                        if (agency.get().getBank()==bank.get()) {
                            BankAccountEntity bankAccount = new BankAccountEntity(code, user, bAType.get(), bank.get(),
                                    agency.get());
                            repo.save(bankAccount);
                            user.addBankAccount(bankAccount);
                            return "Account created successfully.";
                        } else {
                            return "invalid bank/agency combo.";
                        }
                    } else {
                        return "invalid bank/agency code.";
                    }
                } else {
                    return "invalid account type";
                }
            } else {
                return "an account with that code already exists";
            }
        } else {
            return "user account doesn't exist";
        }
    }

    public BankAccountEntity getAccount(long code) {
        if (repo.findByCode(code).isPresent()) {
            return (BankAccountEntity) repo.findByCode(code).get();
        } else {
            return null;
        }
    }

    public List<BankAccountResponse> getAccounts(UserEntity user) {
        List<BankAccountEntity> accounts = repo.findByUserOrderByBalanceDesc(user);
        return accounts.stream()
                .map(this::getAccounts)
                .collect(Collectors.toList());

    }

    public BankAccountResponse getAccounts(@NotNull BankAccountEntity acc) {
        return new BankAccountResponse(acc.getCode(), acc.getBalance(), acc.isActive(),acc.getTransactioncounter(),
         acc.getBank().getName(),
                acc.getAgency().getName(),
                acc.getType());
    }

    public String deleteAccount(UserEntity user, long code) {
        Optional<BankAccountEntity> b = repo.findByCode(code);
        if (b.isPresent()) {
            if (!b.get().isActive() && user == b.get().user) {
                user.getAccounts().remove(b.get());
                repo.delete(b.get());
                return "success";
            }
        }
        return "can't delete this account";
    }

    public String refreshBankAccount(UserEntity user, long code) {
        if (repo.findByCode(code).isPresent()) {
            BankAccountEntity b = repo.findByCode(code).get();
            if (b.getDateHolder().getMonthValue() < LocalDate.now().getMonthValue()) {
                b.setDateHolder(LocalDate.now());
                b.setBalance(b.getBalance() + b.getBalance() * b.getType().getInterestRate());
                b.setTransactioncounter(0);
                repo.save(b);
                return "success";
            } else {
                return b.getDateHolder().getMonthValue() + " " + LocalDate.now().getMonthValue();
            }
        } else {
            return ("Account doesnt exist.");
        }
    }

    public String exists(long code) {
        if (repo.findByCode(code).isPresent()) {
            return "valid";
        }
        return "not";
    }

    public String getReceiver(long code, String email) {
        if (repo.findByCode(code).isPresent()) {
            BankAccountEntity b = repo.findByCode(code).get();
           if (b.user.getEmail().equals(email)){
            return b.user.getFirstName()+" "+b.getUser().getLastName()+" owner of bank account associated to "+
                    b.getBank().getName()+ "'s "+b.getAgency().getName();
           }  
            else {
            return ("check your email/code combination.");
        }

        } else {
            return ("Account doesnt exist.");
        }
    }


}
