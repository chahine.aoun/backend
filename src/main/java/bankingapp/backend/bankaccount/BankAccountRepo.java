package bankingapp.backend.bankaccount;


import bankingapp.backend.user.UserEntity;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


import javax.transaction.Transactional;

@Primary
@Repository
public interface BankAccountRepo extends JpaRepository<BankAccountEntity, UUID> {

    @Transactional
    Optional<BankAccountEntity> findByCode(long code);

    List<BankAccountEntity> findByUserOrderByBalanceDesc(UserEntity user);
}
