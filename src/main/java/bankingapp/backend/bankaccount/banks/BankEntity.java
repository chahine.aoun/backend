package bankingapp.backend.bankaccount.banks;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import bankingapp.backend.bankaccount.BankAccountEntity;
import bankingapp.backend.bankaccount.agencies.AgencyEntity;
import bankingapp.backend.bankaccount.bankaccounttypes.BankAccountTypeEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BankEntity {

    @Id
    @Size(min = 8, max = 11)
    @Pattern(regexp = "^[a-zA-Z]{8}$")
    private String swiftCode ;

    @Column(nullable = false,unique = true)
    @Pattern(regexp = "[A-Za-z\\ '\"'%\\-]+",
    message = "Make sure you entered the name correctly.")
    private String name;
    
    @Column(nullable = false)
    @Pattern(regexp = "[A-Za-z0-9\\.\\s,\\-:\\n]+",
    message = "Make sure you entered the address correctly.")
    private String address;

    @OneToMany
    private Set<BankAccountEntity> accounts;
   
    @OneToMany
    private List<AgencyEntity> agencies;

    @ManyToMany
    private Set<BankAccountTypeEntity> accountTypes;


    public BankEntity(String swiftCode, String name, String address) {
        this.swiftCode=swiftCode;
        this.name=name;
        this.address=address;
        this.agencies=new ArrayList<AgencyEntity>();
        this.accounts=new HashSet<BankAccountEntity>();
        this.accountTypes=new HashSet<BankAccountTypeEntity>();
    }

}
