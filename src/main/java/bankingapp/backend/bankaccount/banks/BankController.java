package bankingapp.backend.bankaccount.banks;

import java.util.List;

import bankingapp.backend.bankaccount.agencies.AgencyResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(path = "/app/adminDash/bank")
@RequiredArgsConstructor
public class BankController {
    private final BankService service;

    @PostMapping(path = "/new")
    public String newBank(@RequestParam("swift code") String swift, @RequestParam("name") String name,
            @RequestParam("address") String address) {
        return service.newBank(swift, name, address);
    }

    @GetMapping(path = "/all")
    public List<BankResponse> getBanks() {
        return service.getBanks();
    }

    @GetMapping(path = "/agencies")
    public List<AgencyResponse> getAgencies(@RequestParam("bank code") String bankCode) {
        return service.getAgencies(bankCode);
    }

    @PostMapping(path = "/addType")
    public String addAccountType(@RequestParam("bank code") String bankCode,@RequestParam("account type") String type) {
        return service.addAccountType(bankCode, type);
    }

    @GetMapping(path = "/accountTypes")
    public List<String> getBanks(@RequestParam("bank code") String code) {
        return service.getTypes(code);
    }

}
