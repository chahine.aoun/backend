package bankingapp.backend.bankaccount.banks;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import bankingapp.backend.bankaccount.agencies.AgencyEntity;
import bankingapp.backend.bankaccount.agencies.AgencyResponse;
import org.springframework.stereotype.Service;

import bankingapp.backend.bankaccount.bankaccounttypes.BankAccountTypeEntity;
import bankingapp.backend.bankaccount.bankaccounttypes.BankAccountTypeRepo;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class BankService {
    private final BankRepo repo;
    private final BankAccountTypeRepo typeRepo;
    
    public String newBank(String swift, String name, String address) {
        BankEntity bank=new BankEntity(swift, name, address);
        repo.save(bank);
        return "success.";
    }

    public List<BankResponse> getBanks() {
        List<BankEntity> l=repo.findAll();
        return l.stream()
        .map(this::getResponse)
        .collect(Collectors.toList());
    }
    public BankResponse getResponse(BankEntity b){
        return new BankResponse(b.getSwiftCode(), b.getName(), b.getAddress());
    }

    public String addAccountType(String bankCode, String type) {
        Optional<BankAccountTypeEntity> AccountType=typeRepo.findBytype(type);
        Optional<BankEntity> bank=repo.findBySwiftCode(bankCode);
        if(AccountType.isPresent() && bank.isPresent()){
            bank.get().getAccountTypes().add(AccountType.get());
            repo.save(bank.get());
            return "success.";
        }
        return "failed.";
    }

    public List<String> getTypes(String code) {
        Optional<BankEntity> bank=repo.findBySwiftCode(code);
        if( bank.isPresent()){
            return bank.get().getAccountTypes().stream()
            .map(this::getTypes)
            .collect(Collectors.toList());
        }
        return null;
    }
    public String getTypes(BankAccountTypeEntity type){
        return type.getType();
    }

    public List<AgencyResponse> getAgencies(String bankCode) {
        List<AgencyEntity> l=repo.findBySwiftCode(bankCode).get().getAgencies();
        return  l.stream()
                .map(this::getAgencyResponse)
                .collect(Collectors.toList());
    }
    public AgencyResponse getAgencyResponse(AgencyEntity b){
        return new AgencyResponse(b.getCode(), b.getName(), b.getAddress());
    }

}
