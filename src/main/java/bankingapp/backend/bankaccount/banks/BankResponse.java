package bankingapp.backend.bankaccount.banks;


import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public class BankResponse {
    private String swiftCode;
    private String name;
    private String address;
}
