package bankingapp.backend.bankaccount.banks;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Primary
@Repository
public interface BankRepo extends JpaRepository<BankEntity, String> {
    @Transactional
    Optional<BankEntity> findBySwiftCode(String swiftCode);
    
}
