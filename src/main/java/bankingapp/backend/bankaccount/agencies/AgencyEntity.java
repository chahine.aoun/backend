package bankingapp.backend.bankaccount.agencies;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import bankingapp.backend.bankaccount.BankAccountEntity;
import bankingapp.backend.bankaccount.banks.BankEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AgencyEntity {

    @Id
    private String code ;
    
    @Column(nullable = false,unique = true)
    @Pattern(regexp = "[A-Za-z\\ '\"'%\\-]+",
    message = "Make sure you entered the name correctly.")
   @NotBlank(message = "name is required.")
    private String name;

    @Column(nullable = false)
    @Pattern(regexp = "[A-Za-z0-9\\.\\s,\\-:\\n]+",
    message = "Make sure you entered the address correctly.")
    private String address;

    @ManyToOne
    private BankEntity bank;

    
    @OneToMany
    private Set<BankAccountEntity> accounts;
    
    public AgencyEntity(String code, String name, String address,BankEntity bank) {
        this.code=code;
        this.name=name;
        this.address=address;
        this.bank=bank;
        this.accounts=new HashSet<BankAccountEntity>();
    }

}