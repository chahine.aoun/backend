package bankingapp.backend.bankaccount.agencies;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Primary
@Repository
public interface AgencyRepo extends JpaRepository<AgencyEntity, String> {
    @Transactional
    Optional<AgencyEntity> findByCode(String code);
}