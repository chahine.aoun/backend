package bankingapp.backend.bankaccount.agencies;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import bankingapp.backend.bankaccount.banks.BankEntity;
import bankingapp.backend.bankaccount.banks.BankRepo;
import bankingapp.backend.bankaccount.banks.BankResponse;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AgencyService {
    private final AgencyRepo repo;
    private final BankRepo bankRepo;

    public String newAgency(String code, String name, String address,String bankCode) {
        Optional<BankEntity> bank = bankRepo.findBySwiftCode(bankCode);
        System.out.println("1");
        if (bank.isPresent()){
            System.out.println("2");
             AgencyEntity agency= new AgencyEntity(code, name, address,bank.get());
            repo.save(agency);
            bank.get().getAgencies().add(agency);
            bankRepo.save(bank.get());
            return "success.";}
        else {
            return "bank code is incorrect.";
        }
    }


    public List<AgencyResponse> getAgencies() {
        List<AgencyEntity> l=repo.findAll();
        return l.stream()
        .map(this::getResponse)
        .collect(Collectors.toList());
    }
    public AgencyResponse getResponse(AgencyEntity b){
        return new AgencyResponse(b.getCode(), b.getName(), b.getAddress());
    }
    public BankResponse getBankResponse(BankEntity b){
        return new BankResponse(b.getSwiftCode(), b.getName(), b.getAddress());
    }

}
