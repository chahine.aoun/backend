package bankingapp.backend.bankaccount.agencies;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import bankingapp.backend.bankaccount.banks.BankResponse;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(path = "/app/adminDash/agency")
@RequiredArgsConstructor
public class AgencyController {
    private final AgencyService service;

    @PostMapping(path = "/new")
    public String newagency(@RequestParam("code") String code,
                            @RequestParam("name") String name,
            @RequestParam("address") String address,
            @RequestParam("bank code") String bankCode) {
        return service.newAgency(code, name, address,bankCode);
    }

    @GetMapping(path = "/all")
    public List<AgencyResponse> getAgencies() {
        return service.getAgencies();
    }
}