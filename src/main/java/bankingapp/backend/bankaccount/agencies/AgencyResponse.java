package bankingapp.backend.bankaccount.agencies;

import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public class AgencyResponse {
    
    private String code;
    private String name;
    private String address;

}