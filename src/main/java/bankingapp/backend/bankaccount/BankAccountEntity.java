package bankingapp.backend.bankaccount;

import bankingapp.backend.bankaccount.agencies.AgencyEntity;
import bankingapp.backend.bankaccount.bankaccounttypes.BankAccountTypeEntity;
import bankingapp.backend.bankaccount.banks.BankEntity;
import bankingapp.backend.bankaccount.transactions.TransactionEntity;
import bankingapp.backend.user.UserEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import java.time.LocalDate;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class BankAccountEntity {
    @Id
    private long code;
    @Column(nullable = false)
    private double balance;
    @Column(nullable = false)
    private boolean active;
    @Column(nullable = false)
    private LocalDate dateHolder;

    @ManyToOne
    private BankAccountTypeEntity type;

    @Column(nullable = false)
    private int transactioncounter;

    @ManyToOne
    private BankEntity bank;
    @ManyToOne
    private AgencyEntity agency;

    @ManyToMany
    private List<TransactionEntity> transactions;

    @ManyToOne
    UserEntity user;

    public BankAccountEntity(long code, UserEntity user, BankAccountTypeEntity type, BankEntity bank,
            AgencyEntity agency) {
        this.bank = bank;
        this.agency = agency;
        this.code = code;
        this.balance = 0;
        this.transactioncounter = 0;
        this.active = false;
        this.user = user;
        this.type = type;
        this.dateHolder = LocalDate.now();
    }
}
