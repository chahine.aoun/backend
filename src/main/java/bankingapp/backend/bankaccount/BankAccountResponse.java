package bankingapp.backend.bankaccount;


import bankingapp.backend.bankaccount.bankaccounttypes.BankAccountTypeEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public class BankAccountResponse {

    private long code;
    private double balance;
    private boolean active;
    private int transactionCounter;
    private String bank;
    private String agency;
    private BankAccountTypeEntity type;
}
