package bankingapp.backend.bankaccount;


import bankingapp.backend.user.UserEntity;
import bankingapp.backend.user.UserService;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import java.util.List;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;


@RestController
@RequestMapping(path = "/app/account/bankAccount")
@RequiredArgsConstructor
public class BankAccountController {
    private final BankAccountService bankAccountService;
    private final UserService userService;

    @PostMapping
    public String addAccount(HttpServletRequest request,
                             @RequestParam("type") String type,@RequestParam("swift code") String swift, @RequestParam("agency code") String agencyCode) throws Exception {
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String refreshToken = authorizationHeader.substring("Bearer ".length());
            Algorithm algorithm = Algorithm.HMAC256("jwtSecret".getBytes());
            JWTVerifier verifier = JWT.require(algorithm).build();
            DecodedJWT decodedJWT = verifier.verify(refreshToken);
            String email = decodedJWT.getSubject();
            return bankAccountService.addAccount(email, type,swift,agencyCode);
        } else {
            throw new Exception("Token is missing");
        }
    }

    @GetMapping(path = "/number")
    public int count(HttpServletRequest request) throws Exception {
        UserEntity user =extractToken(request);
        return bankAccountService.count(user);
    }
    @GetMapping(path = "/exists")
    public String exists(@RequestParam("code") long code) throws Exception {
        return bankAccountService.exists(code);
    }
    @GetMapping(path = "/all")
    public List<BankAccountResponse> GetAccounts(HttpServletRequest request) throws Exception {
        UserEntity user =extractToken(request);
        return bankAccountService.getAccounts(user);
    }
    @DeleteMapping
    public String deleteAccount(HttpServletRequest request,@RequestParam("code") long code)throws Exception {
        UserEntity user =extractToken(request);
        return bankAccountService.deleteAccount(user,code);
    }
    
    @GetMapping(path = "/refreshAccount")
    public String refreshBankAccount(HttpServletRequest request,@RequestParam("code") long code) throws Exception {
        UserEntity user =extractToken(request);
        return bankAccountService.refreshBankAccount(user,code);
    }

    @GetMapping(path = "/checkReceiver")
    public String getReceiver(@RequestParam("code") long code
    ,@RequestParam("email") String email) throws Exception {
        return bankAccountService.getReceiver(code,email);
    }


    private UserEntity extractToken(HttpServletRequest request)throws Exception{
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String refreshToken = authorizationHeader.substring("Bearer ".length());
            Algorithm algorithm = Algorithm.HMAC256("jwtSecret".getBytes());
            JWTVerifier verifier = JWT.require(algorithm).build();
            DecodedJWT decodedJWT = verifier.verify(refreshToken);
            String email = decodedJWT.getSubject();
            return (UserEntity) userService.loadUserByUsername(email);
        } else {
            throw new Exception("Token is missing");
        }
    }
}
