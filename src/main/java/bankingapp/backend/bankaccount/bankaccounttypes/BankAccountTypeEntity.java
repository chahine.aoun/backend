package bankingapp.backend.bankaccount.bankaccounttypes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import bankingapp.backend.bankaccount.BankAccountEntity;
import bankingapp.backend.bankaccount.banks.BankEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BankAccountTypeEntity {

    @Id
    private String type;
    @Column(nullable = false)
    private double transactionFee;
    @Column(nullable = false)
    private double interestRate;
    @Column(nullable = false)
    private int transactionLimit;
    @Column(nullable = false)
    private int minBalance;

    @OneToMany
    private List<BankAccountEntity> accounts;

    @ManyToMany
    private Set<BankEntity> banks;

    public BankAccountTypeEntity(String type, double tFee, double iRate, int tLimit, int minBalance) {
        this.accounts = new ArrayList<BankAccountEntity>();
        this.type = type;
        this.transactionFee = tFee;
        this.interestRate = iRate;
        this.transactionLimit = tLimit;
        this.minBalance = minBalance;
        banks=new HashSet<BankEntity>();
    }
}
