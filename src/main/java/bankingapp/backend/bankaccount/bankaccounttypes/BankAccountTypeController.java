package bankingapp.backend.bankaccount.bankaccounttypes;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(path = "/app/adminDash/accountType")
@RequiredArgsConstructor
public class BankAccountTypeController {
    private final BankAccountTypeService service; 


    @PostMapping(path = "/new")
    public String newBankAccouStringType(@RequestParam("type") String type, @RequestParam("transaction fee") double tFee,
    @RequestParam("interest rate") double iRate,@RequestParam("transaction limit") int tLimit,@RequestParam("min balance") int minBalance){
        return service.newType(type,tFee,iRate,tLimit,minBalance);
        }
}
