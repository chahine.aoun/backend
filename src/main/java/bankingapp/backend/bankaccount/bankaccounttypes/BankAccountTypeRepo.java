package bankingapp.backend.bankaccount.bankaccounttypes;


import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Primary
@Repository
public interface BankAccountTypeRepo extends JpaRepository<BankAccountTypeEntity, String> {

    @Transactional
    Optional<BankAccountTypeEntity> findBytype(String type);
}

