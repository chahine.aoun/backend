package bankingapp.backend.bankaccount.bankaccounttypes;


import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class BankAccountTypeService {
    private final BankAccountTypeRepo repo;
    
    public String newType(String type, double tFee, double iRate, int tLimit, int minBalance) {
        if(repo.findBytype(type).isPresent()){
            return "This type already exists.";
        }
            BankAccountTypeEntity accountType =new BankAccountTypeEntity(type, tFee, iRate, tLimit, minBalance);
            repo.save(accountType);
            return "success.";
    }
    
}
