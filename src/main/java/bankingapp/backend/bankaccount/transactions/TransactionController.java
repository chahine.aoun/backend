package bankingapp.backend.bankaccount.transactions;


import bankingapp.backend.bankaccount.BankAccountEntity;
import bankingapp.backend.bankaccount.BankAccountService;
import bankingapp.backend.user.UserEntity;
import bankingapp.backend.user.UserService;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


import static org.springframework.http.HttpHeaders.AUTHORIZATION;

import java.util.List;

@RestController
@RequestMapping(path = "/app/account/transaction")
@RequiredArgsConstructor
public class TransactionController {
    private final UserService userService;
    private final TransactionService service;
    private final BankAccountService bankAccountService;

    @PostMapping
    public String sendMoney(HttpServletRequest request,@RequestParam("amount") double amount,@RequestParam("sender") long s,@RequestParam("receiver") long r) throws Exception {
          UserEntity user =extractToken(request);
            return service.sendMoney(user,amount,s,r);
    }

    @GetMapping
    public List<TransactionResponse> getAllTransactions(HttpServletRequest request) throws Exception{
            UserEntity user =extractToken(request);
            return service.getAllTransactions(user);
    }
    @GetMapping(path = "/specific")
    public List<TransactionResponse> getTransactions(HttpServletRequest request,@RequestParam("code") long code) throws Exception {
        UserEntity user =extractToken(request);
        BankAccountEntity b =bankAccountService.getAccount(code);
        if(b.getUser()==user){
            return service.getTransactions(b);
        }
        else{
            throw new Exception("user and bankAccount dont match.");
        }
    }

    private UserEntity extractToken(HttpServletRequest request)throws Exception{
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String refreshToken = authorizationHeader.substring("Bearer ".length());
            Algorithm algorithm = Algorithm.HMAC256("jwtSecret".getBytes());
            JWTVerifier verifier = JWT.require(algorithm).build();
            DecodedJWT decodedJWT = verifier.verify(refreshToken);
            String email = decodedJWT.getSubject();
            return (UserEntity) userService.loadUserByUsername(email);
        } else {
            throw new Exception("Token is missing");
        }
    }
}
