package bankingapp.backend.bankaccount.transactions;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import bankingapp.backend.bankaccount.BankAccountEntity;
import bankingapp.backend.bankaccount.BankAccountRepo;
import bankingapp.backend.bankaccount.bankaccounttypes.BankAccountTypeEntity;
import bankingapp.backend.user.UserEntity;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class TransactionService {
    private final BankAccountRepo bankAccountRepo;
    private final TransactionRepo repo;

    @Transactional
    public String sendMoney(UserEntity user, double amount, long sender, long receiver) {
        if (amount > 0 && sender != receiver) {
            Optional<BankAccountEntity> senderr = bankAccountRepo.findByCode(sender);
            Optional<BankAccountEntity> receiverr = bankAccountRepo.findByCode(receiver);
            if (senderr.isPresent() && receiverr.isPresent()) {
                BankAccountEntity s = senderr.get();
                BankAccountEntity r = receiverr.get();
                if (s.getUser() == user && s.isActive() && r.isActive()) {
                    BankAccountTypeEntity type = s.getType();
                    if (s.getBalance() - (amount + amount * type.getTransactionFee()) > 0) {
                       if(type.getTransactionLimit()>0){
                        if (s.getTransactioncounter() < type.getTransactionLimit()) {
                            s.setBalance(s.getBalance() - (amount + amount * type.getTransactionFee()));
                            r.setBalance(r.getBalance() + amount);
                            TransactionEntity transaction = new TransactionEntity(amount, s, r);
                            repo.save(transaction);
                            s.getTransactions().add(transaction);
                            s.setTransactioncounter(s.getTransactioncounter()+1);
                            r.getTransactions().add(transaction);
                            bankAccountRepo.save(s);
                            bankAccountRepo.save(r);
                            return "Transaction successful.";
                        } else {
                            return " you have reached your limit of allowed transactions for this month.";
                        }}else{
                            s.setBalance(s.getBalance() - (amount + amount * type.getTransactionFee()));
                            r.setBalance(r.getBalance() + amount);
                            TransactionEntity transaction = new TransactionEntity(amount, s, r);
                            repo.save(transaction);
                            s.getTransactions().add(transaction);
                            r.getTransactions().add(transaction);
                            bankAccountRepo.save(s);
                            bankAccountRepo.save(r);
                            return "Transaction successful."; 
                        }
                    } else {
                        return "insufficient funds.";
                    }
                } else {
                    return "The receiver's bank account hasn't been activated yet.";
                }
            } else {
                return "Bank account doesn't exist.";
            }
        }
        return "Transaction failed.";
    }

    @Transactional
    public List<TransactionResponse> getAllTransactions(UserEntity user) {
        List<List<TransactionResponse>> tranLists = user.getAccounts().stream()
                .map(this::getTransactions)
                .collect(Collectors.toList());
        List<TransactionResponse> transactionResponses = new ArrayList<TransactionResponse>();
        tranLists.forEach((l) -> {
            if (l != null)
                transactionResponses.addAll(l);
        });
        for (int i = 0; i < transactionResponses.size(); i++) {
            for (int j = i + 1; j < transactionResponses.size(); j++) {
                if (transactionResponses.get(i).getDate().equals(transactionResponses.get(j).getDate()) &&
                        transactionResponses.get(i).getSender() == transactionResponses.get(j).getSender()) {
                    transactionResponses.remove(j);
                }
            }
        }
        return (transactionResponses);
    }

    public List<TransactionResponse> getTransactions(BankAccountEntity b) {
        if (b.isActive()) {
            List<TransactionResponse> l = new ArrayList<TransactionResponse>();
            b.getTransactions().forEach((n) -> {
                l.add(getResponse(n, b.getCode()));
            });
            return l;
        }
        return null;
    }

    public TransactionResponse getResponse(TransactionEntity t, long b) {
        if (t.getDate().isAfter(LocalDateTime.now().minusMonths(1))) {
            return new TransactionResponse(t.getDate(), t.getSender(), t.getReceiver(), t.getAmount(),
                    b == t.getSender());
        }
        return null;
    }
}
