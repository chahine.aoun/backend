package bankingapp.backend.bankaccount.transactions;


import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public class TransactionResponse {
    private LocalDateTime date;
    private long sender;
    private long receiver;
    private double amount; 
    private boolean sent;
}
