package bankingapp.backend.bankaccount.transactions;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;


import bankingapp.backend.bankaccount.BankAccountEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.validation.constraints.*;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class TransactionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int number;
    @Column(nullable = false)
    private LocalDateTime date;
    @Column(nullable = false)
    private long sender;
    @Column(nullable = false)
    private long receiver;
    
    @Column(nullable = false)
    @ManyToMany(fetch = FetchType.EAGER)
    private List<BankAccountEntity> accounts=new ArrayList<BankAccountEntity>();


    @Column(nullable = false)
    @Positive
    private double amount;

    public TransactionEntity(double a,BankAccountEntity s,BankAccountEntity r){
        this.accounts.add(s);
        this.sender=s.getCode();
        this.accounts.add(r);
        this.receiver=r.getCode();
        this.amount=a;
        this.date=LocalDateTime.now();
    }
}
