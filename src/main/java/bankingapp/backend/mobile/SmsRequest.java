package bankingapp.backend.mobile;


import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class SmsRequest {
    private final String phoneNumber;
    private final String message;

    public SmsRequest( String phoneNumber,String appSig) {
        this.phoneNumber = phoneNumber;
        this.message = "your verification code is: "+(100000 + (int)(Math.random() * ((999999 - 100000) + 1)))+"       "+appSig;
    }

}