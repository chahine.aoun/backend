package bankingapp.backend.mobile;


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/app/phone")
@RequiredArgsConstructor
public class MobileVerificationController {
    private final MobileVerificationService service;

    @PostMapping
    public void sendSms(@RequestParam("phoneNumber") String number,@RequestParam("appSig") String appSig,
                        HttpServletResponse response) {
        SmsRequest smsRequest=new SmsRequest(number,appSig);
        response.setHeader("outcome",service.sendSms(smsRequest));
        System.out.println(appSig+"       aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    }
    @PostMapping(path = "/verify")
    public void verifyCode(@RequestParam("phoneNumber")String phoneNumber, @RequestParam("code")String code,
                             HttpServletResponse response){
        response.setHeader("outcome", service.confirmCode(phoneNumber,code));
    }
}

