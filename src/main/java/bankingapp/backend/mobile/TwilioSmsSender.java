package bankingapp.backend.mobile;


import bankingapp.backend.mobile.config.TwilioConfiguration;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.MessageCreator;
import com.twilio.type.PhoneNumber;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service("twilio")
@RequiredArgsConstructor
public class TwilioSmsSender  {
    private final TwilioConfiguration twilioConfiguration;


    public void sendSms(SmsRequest smsRequest) {

            PhoneNumber to = new PhoneNumber(smsRequest.getPhoneNumber());
            PhoneNumber from = new PhoneNumber(twilioConfiguration.getNumber());
            String message = smsRequest.getMessage();
            MessageCreator creator = Message.creator(to, from, message);
            creator.create();
        }
    }


