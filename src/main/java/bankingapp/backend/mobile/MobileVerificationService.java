package bankingapp.backend.mobile;

import bankingapp.backend.registration.token.ConfirmationToken;
import bankingapp.backend.registration.token.ConfirmationTokenService;
import bankingapp.backend.user.UserEntity;
import bankingapp.backend.user.UserRepo;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class MobileVerificationService {
    private final TwilioSmsSender smsSender;
    private final ConfirmationTokenService confirmationTokenService;
    private final UserRepo userRepo;

    public String sendSms(SmsRequest smsRequest) {
        String token = smsRequest.getMessage().substring("your verification code is: ".length());
        if (userRepo.findByPhoneNumber(smsRequest.getPhoneNumber()).isPresent()) {
            UserEntity user = userRepo.findByPhoneNumber(smsRequest.getPhoneNumber()).get();
            ConfirmationToken confirmationToken = new ConfirmationToken(
                    token.substring(0, 6),
                    LocalDateTime.now(),
                    LocalDateTime.now().plusMinutes(5),
                    user.getId(),
                    smsRequest.getPhoneNumber());
            confirmationTokenService.saveConfirmationToken(confirmationToken);
            try {
                smsSender.sendSms(smsRequest);
            } catch (Exception exception) {
                return (smsRequest.getPhoneNumber() + " is not a valid number.");
            }
        } else {
            ConfirmationToken confirmationToken = new ConfirmationToken(
                    token.substring(0, 6),
                    LocalDateTime.now(),
                    LocalDateTime.now().plusMinutes(5),
                    smsRequest.getPhoneNumber());
            confirmationTokenService.saveConfirmationToken(confirmationToken);
            try {
                smsSender.sendSms(smsRequest);
            } catch (Exception exception) {
                return (smsRequest.getPhoneNumber() + " is not a valid number.");
            }
        }
        return "The code was sent to your number.";
    }

    @Transactional
    public String confirmCode(String phoneNumber, String code) {
        if (confirmationTokenService.getToken(phoneNumber, code).isPresent()) {
            ConfirmationToken confirmationToken = confirmationTokenService.getToken(phoneNumber, code).get();
            if (confirmationToken.getConfirmedAt() != null) {
                return ("This is an old code");
            }
            if (confirmationToken.getExpiresAt().isBefore(LocalDateTime.now())) {
                return ("This code has expired");
            }
            confirmationTokenService.setConfirmedAt(code);
            return "Confirmed successfully";
        } else {
            return "Wrong code";
        }
    }

    public boolean isPhoneNumberValid(String phone) {

        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber phoneNumber = null;

        try {
            phoneNumber = phoneUtil.parse(phone, "TN");
        } catch (NumberParseException e) {
            e.printStackTrace();
        }
        return phoneUtil.isValidNumber(phoneNumber);
    }

}
