package bankingapp.backend.user;

import bankingapp.backend.bankaccount.BankAccountEntity;
import bankingapp.backend.files.FileEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor
@Entity
public class UserEntity implements UserDetails {

    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @GeneratedValue(generator = "uuid2")
    @Id
    private UUID id;
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    @Column(unique = true,nullable = false)
    private String email;
    @Column(nullable = false)
    private String phoneNumber;
    @Column(nullable = false)
    private String password;
    @Enumerated(EnumType.STRING)
    @Column(updatable = false)
    private Role role;
    private Boolean locked = false;
    private Boolean enabled = false;
    @OneToOne(cascade = CascadeType.ALL)
    private FileEntity idPhoto;

    @Column(nullable = false)
    private String typeOfID;
    @Column(nullable = false)
    private String idNumber;
    private String referralCode;
    private String expirationDate;
    @Column(nullable = false)
    private String birthday;
    private String gender;
    @Column(nullable = false)
    private String countryOfResidence;
    @Column(nullable = false)
    private String address;
    @Column(nullable = false)
    private String pin;

    @OneToMany
    private List<BankAccountEntity> accounts ;

    public UserEntity(String firstName,
                      String lastName,
                      String email,
                      String phoneNumber,
                      String password,
                      Role role,
                      FileEntity file,
                      String typeOfId,
                      String idNumber,
                      String birthday,
                      String countryOfResidence,
                      String address,
                      String referralCode,
                      String expirationDate,
                      String gender,String pin) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber=phoneNumber;
        this.password = password;
        this.role =role;
        this.idPhoto=file;
        this.typeOfID=typeOfId;
        this.idNumber=idNumber;
        this.birthday=birthday;
        this.countryOfResidence=countryOfResidence;
        this.address=address;
        this.referralCode=referralCode;
        this.expirationDate=expirationDate;
        this.gender=gender;
        this.pin=pin;
    }
    public UserEntity(Role role){
        this.role=role;
    }

    public void addBankAccount(BankAccountEntity bankAccount){
        this.accounts.add(bankAccount);
    }
    
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        SimpleGrantedAuthority authority =
                new SimpleGrantedAuthority(role.name());
        return Collections.singletonList(authority);
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !locked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
