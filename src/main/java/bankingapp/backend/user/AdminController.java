package bankingapp.backend.user;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/app/admin")
@RequiredArgsConstructor
public class AdminController {
    private final UserService userService;

    @GetMapping("/users")
    public List<UserResponse> getUsers() {
        return userService.getUsers();
    }
    @GetMapping("/user/email")
    public UserResponse getUser(@RequestParam("email") @NonNull String email) {
        return userService.getUser(email);
    }
    @GetMapping("/user/phone")
    public UserResponse getUserByPhone(@RequestParam("phone")@NonNull String phone) {
        return userService.getUserByPhone(phone);
    }
}
