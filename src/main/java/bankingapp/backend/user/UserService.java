package bankingapp.backend.user;

import bankingapp.backend.email.EmailSender;
import bankingapp.backend.email.EmailService;
import bankingapp.backend.files.FileRepo;
import bankingapp.backend.registration.token.ConfirmationToken;
import bankingapp.backend.registration.token.ConfirmationTokenService;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserService implements UserDetailsService {

    private static final String USER_NOT_FOUND_MSG = "user with email %s not found";
    private final UserRepo userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final ConfirmationTokenService confirmationTokenService;
    private final EmailSender emailSender;
    private final EmailService emailService;
    private final FileRepo fileRepo;


    @Override
    public UserDetails loadUserByUsername(String email)
            throws UsernameNotFoundException {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException(
                        String.format(USER_NOT_FOUND_MSG, email)));
    }

    public List<String> signUpUser(@NotNull UserEntity user) {
        boolean emailIsUsed = userRepository
                .findByEmail(user.getEmail())
                .isPresent();
        boolean phoneIsUsed = userRepository
                .findByPhoneNumber(user.getPhoneNumber())
                .isPresent();

        if (emailIsUsed) {
            UserEntity use = userRepository.findByEmail(user.getEmail()).get();

            if (confirmationTokenService.getToken(use.getId()).get().getConfirmedAt() == null) {

                String token = UUID.randomUUID().toString();
                ConfirmationToken confirmationToken = new ConfirmationToken(
                        token,
                        LocalDateTime.now(),
                        LocalDateTime.now().plusMinutes(15),
                        use.getId());

                confirmationTokenService.saveConfirmationToken(confirmationToken);
                String link = "http://localhost:8081/app/reg/confirm?token=" + token;
                emailSender.send(
                        use.getEmail(),
                        emailService.buildEmail(use.getFirstName(), link));

                throw new IllegalStateException("email already taken, please confirm that account");
            }
            throw new IllegalStateException("email already taken");
        }
        if (phoneIsUsed) {
            throw new IllegalStateException("phone number already taken.");
        }
        String pin = (1000 + (int) (Math.random() * ((9999 - 1000) + 1))) + "";
        String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
        String encodedPin = bCryptPasswordEncoder.encode(pin);

        user.setPassword(encodedPassword);
        user.setPin(encodedPin);

        userRepository.save(user);

        String token = UUID.randomUUID().toString();

        ConfirmationToken confirmationToken = new ConfirmationToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(15),
                user.getId());

        confirmationTokenService.saveConfirmationToken(confirmationToken);
        List<String> l = new ArrayList<>();
        l.add(token);
        l.add(pin);
        return l;
    }

    public void enableUser(UUID  id) {
        userRepository.enableAppUser(id);
    }

    public List<UserResponse> getUsers() {
        List<UserEntity> users = userRepository.findAll();
        return users.stream()
                .map(this::getUser)
                .collect(Collectors.toList());
    }

    public UserResponse getUser(@NotNull UserEntity user) {
        if (user.getRole() != Role.DEV) {
            return new UserResponse(user.getFirstName(), user.getLastName(), user.getEmail(), user.getPhoneNumber(),
                    user.getGender(), user.getTypeOfID(), user.getIdNumber(), user.getReferralCode(),
                    user.getExpirationDate(), user.getBirthday(), user.getCountryOfResidence(),
                    user.getAddress());
        } else {
            return new UserResponse(null, null, null, null, null, null);
        }
    }

    public UserResponse getUser(String email) {
        UserEntity user = userRepository.findByEmail(email).get();
        return getUser(user);
    }

    public UserResponse getUserByPhone(String phone) {
        UserEntity user = userRepository.findByPhoneNumber(phone).get();
        return getUser(user);
    }

    public boolean emailTest(String email) {
        return userRepository.findByEmail(email).isPresent();
    }

    public boolean phoneTest(String phone) {
        return userRepository.findByPhoneNumber(phone).isPresent();
    }

    public String testPin(UserEntity user, @NonNull String pin) {
        if (bCryptPasswordEncoder.matches(pin, user.getPin())) {
            return "correct";
        }
        return "false";
    }

    public String changePassword(UserEntity user, @NonNull String password) {

        if (password.matches("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$")) {
            String encodedPassword = bCryptPasswordEncoder.encode(password);
            user.setPassword(encodedPassword);
            userRepository.save(user);
            return "success";
        }
        return "failed";
    }

    public String update(UserEntity user, UserResponse profile, @NonNull MultipartFile file) throws IOException {
try{
            user.setFirstName(profile.getFirstName());
            user.setLastName(profile.getLastName());
            user.setPhoneNumber(profile.getPhoneNumber());
            user.setGender(profile.getGender());
            user.setTypeOfID(profile.getTypeOfID());
            user.setIdNumber(profile.getIdNumber());
            user.setReferralCode(profile.getReferralCode());
            user.setExpirationDate(profile.getExpirationDate());
            user.setBirthday(profile.getBirthday());
            user.setCountryOfResidence(profile.getCountryOfResidence());
            user.setAddress(profile.getAddress());
            fileRepo.updateFile(user.getIdPhoto().getId(), file.getBytes(), file.getSize(), file.getContentType());
            userRepository.save(user);
            return "success.";
        } catch(Exception e) {
            return e.getMessage();
        }
    }
    public String update(UserEntity user, UserResponse profile) throws IOException {
      try {
            user.setFirstName(profile.getFirstName());
            user.setLastName(profile.getLastName());
            user.setPhoneNumber(profile.getPhoneNumber());
            user.setGender(profile.getGender());
            user.setBirthday(profile.getBirthday());
            user.setCountryOfResidence(profile.getCountryOfResidence());
            user.setAddress(profile.getAddress());
            userRepository.save(user);
            return "success.";
        } catch(Exception e) {
            return e.getMessage();
        }
    }
}
