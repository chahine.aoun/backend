package bankingapp.backend.user;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import bankingapp.backend.filter.JwtConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;



import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping(path = "/app/account")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final JwtConfig jwtConfig;

   /* @Value("${jwt.secret}")
    private String jwtSecret;*/


    @PostMapping("/token/refresh")
    public void refreshhToken(HttpServletRequest request, HttpServletResponse response, @RequestParam("token")@NonNull String token) throws IOException {
        String authorizationHeader = token;
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            try {
                String refreshToken = authorizationHeader.substring("Bearer ".length());
                Algorithm algorithm = Algorithm.HMAC256("jwtSecret".getBytes());
                //TODO work on decentralized jwt and use of public/private keys
                JWTVerifier verifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = verifier.verify(refreshToken);
                String email = decodedJWT.getSubject();
                UserEntity user =(UserEntity) userService.loadUserByUsername(email);
                String accessToken = JWT.create()
                        .withSubject(user.getUsername())
                        .withExpiresAt(new Date(System.currentTimeMillis()+10*60*1000))
                        .withIssuedAt(new Date(System.currentTimeMillis()))
                        .withClaim("roles",user.getRole().toString())
                        .sign(algorithm);
                response.setHeader("access_token", accessToken);
            }catch (Exception exception) {
                response.setHeader("error", exception.getMessage());
                response.setStatus(FORBIDDEN.value());
                Map<String, String> error = new HashMap<>();
                error.put("error_message", exception.getMessage());
                response.setContentType(MimeTypeUtils.APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), error);
            }
        } else {
            throw new RuntimeException("Refresh token is missing");
        }
    }
    @GetMapping("/token/check")
    public String testToken(HttpServletRequest request, @RequestParam("token")@NonNull String token){
        Algorithm algorithm = Algorithm.HMAC256("jwtSecret".getBytes());
        JWTVerifier verifier = JWT.require(algorithm).build();
        DecodedJWT decodedJWT = verifier.verify(token);
        if(decodedJWT.getExpiresAt().after(new Date(System.currentTimeMillis()))){
            return "Session is still active.";
        }
        return "Session has expired.";
    }
    @GetMapping("/pin/check")
    public String testPin(HttpServletRequest request, @RequestParam("pin")@NonNull String pin) throws Exception{
        UserEntity user=extractToken(request);
        return userService.testPin(user,pin);
    }

    @GetMapping("/profile")
    public UserResponse getProfile(HttpServletRequest request) throws Exception{
        UserEntity user=extractToken(request);
        return userService.getUser(user);
    }

    
    @PostMapping("/changePassword")
    public String changePassword(HttpServletRequest request, @RequestParam("password")@NonNull String password) throws Exception{
        UserEntity user=extractToken(request);
        return userService.changePassword(user,password);
    }
    @PostMapping("/changeProfileInformation")
    public String changeProfile(HttpServletRequest request, @RequestParam("information")@NonNull String info, @RequestParam("file")@NonNull MultipartFile file) throws Exception{
        UserEntity user=extractToken(request);
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode node = mapper.readTree(info);
            UserResponse profile = mapper.treeToValue(node, UserResponse.class);
                return userService.update(user,profile,file);
        } catch (Exception e) {
            return e.getMessage();
        }
    }
    @PostMapping("/changeProfileInformation2")
    public String changeProfile(HttpServletRequest request, @RequestParam("information")@NonNull String info) throws Exception{
        UserEntity user=extractToken(request);
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode node = mapper.readTree(info);
            UserResponse profile = mapper.treeToValue(node, UserResponse.class);
                return userService.update(user,profile);
        } catch (Exception e) {
            return e.getMessage();
        }
    }
    


    
    private UserEntity extractToken(HttpServletRequest request)throws Exception{
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String refreshToken = authorizationHeader.substring("Bearer ".length());
            Algorithm algorithm = Algorithm.HMAC256("jwtSecret".getBytes());
            JWTVerifier verifier = JWT.require(algorithm).build();
            DecodedJWT decodedJWT = verifier.verify(refreshToken);
            String email = decodedJWT.getSubject();
            return (UserEntity) userService.loadUserByUsername(email);
        } else {
            throw new Exception("Token is missing");
        }
    }
}
