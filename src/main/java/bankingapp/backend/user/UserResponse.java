package bankingapp.backend.user;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import bankingapp.backend.files.FileResponse;
import lombok.*;



@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserResponse {
    @Pattern(regexp = "[A-Za-z\\ '\"'%\\-]+",
            message = "Make sure you entered your first name correctly.")
    @NotBlank(message = "First name is required.")
    @Size(min = 3, max = 50, message = "Name must be between 3 and 50 characters")
    private String firstName;
    
    @Pattern(regexp = "[A-Za-z\\ '\"'%\\-]+",
            message = "Make sure you entered your last name correctly.")
    @NotBlank(message = "Last name is required.")
    @Size(min = 3, max = 50, message = "Last name must be between 3 and 50 characters")
    private String lastName;

    @Email(message = "Make sure you typed your email address correctly.")
    private String email;

    @NotBlank(message = "Phone number is required.")
    private String phoneNumber;

    @Pattern(regexp = "(male)|(female)",
            message = "Make sure you entered your gender correctly.")
    @NotBlank(message = "Please specify your gender.")
    private String gender;

    @Pattern(regexp = "(ID card)|(Passport)",
            message = "Make sure you entered your ID type correctly.")
    @NotBlank(message = "Please specify you id type.")
    private String typeOfID;

    @Pattern(regexp = "[0-9\\ \"'%\\-]+",
    message = "Make sure you entered your ID's number correctly.")
@NotBlank(message = "The id number is required.")
    private String idNumber;

    
    @Pattern(regexp = "[0-9\\ \"'%\\-]+",
            message = "Make sure you entered your referral code correctly.")
    private String referralCode;

    @NotBlank(message = "Please specify your ID's expiration date.")
    @Pattern(regexp = "^\\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$",
           message = "make sure you entered the expiration date correctly.")
    private String expirationDate;

    
    @NotBlank(message = "Please specify your birthdate.")
    @Pattern(regexp = "^\\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$",
          message = "Make sure you entered your birth date correctly.")
    private String birthday;

    @Pattern(regexp = "[A-Za-z\\ \"'%\\-]+",
    message = "Make sure you entered your country of residence correctly.")
@NotBlank(message = "Please specify your current country of residence.")
    private String countryOfResidence;

    @Pattern(regexp = "[A-Za-z0-9\\.\\s,\\-:\\n]+",
    message = "Make sure you entered your address correctly.")
@NotBlank(message = "Please specify your address.")
    private String address;

    public UserResponse( String firstName,
                          String lastName,
                          String email,
                          String phoneNumber,
                          String gender,
                          FileResponse idPhoto){
        this.firstName=firstName;
        this.lastName=lastName;
        this.email=email;
        this.phoneNumber=phoneNumber;
        this.gender=gender;
    }
}
