package bankingapp.backend.security.config;

import bankingapp.backend.filter.AuthenticationFilter;
import bankingapp.backend.filter.AuthorizationFilter;
import bankingapp.backend.filter.JwtConfig;
import bankingapp.backend.user.Role;
import bankingapp.backend.user.UserService;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
@AllArgsConstructor
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserService userService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        AuthenticationFilter customAuthenticationFilter = new AuthenticationFilter(authenticationManagerBean(),new JwtConfig());
        customAuthenticationFilter.setFilterProcessesUrl("/app/login");
        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(STATELESS);
        http.authorizeRequests().antMatchers(HttpMethod.GET,"/app/adminDash/bank/agencies"
                        ,"/app/adminDash/bank/all","/app/adminDash/agency/all")
        .permitAll();
        http.authorizeRequests().antMatchers(HttpMethod.POST,"/app/reg/admin/**",
                        "/app/adminDash/accountType/**"
                        ,"/app/adminDash/bank/**"
                        ,"/app/adminDash/agency/**","/app/reg/admin")
                .hasAnyAuthority(Role.SUPERADMIN.name());
        http.authorizeRequests().antMatchers(HttpMethod.GET,"/app/admin/users","/app/account/files")
                .hasAnyAuthority(Role.SUPERADMIN.name());
        http.authorizeRequests().antMatchers("/app/account/files/{id}","/app/admin/user")
                .hasAnyAuthority(Role.SUPERADMIN.name(),Role.ADMIN.name());
        http.authorizeRequests().antMatchers("/app/account/token/**","/app/account/bankAccount/exists").permitAll();
        http.authorizeRequests().antMatchers("/app/account/**")
                .hasAnyAuthority(Role.USER.name(),Role.SUPERADMIN.name(),Role.ADMIN.name());
        http.authorizeRequests().antMatchers("/app/login/**",
                "/app/reg/**","/app/phone/**").permitAll();
        http.authorizeRequests().anyRequest().authenticated();
        http.addFilter(customAuthenticationFilter);
        http.addFilterBefore(new AuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
    }
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
