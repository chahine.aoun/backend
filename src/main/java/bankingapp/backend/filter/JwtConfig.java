package bankingapp.backend.filter;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("jwt")
@NoArgsConstructor
@Getter
@Setter
public class JwtConfig {
    private String secret;
}
