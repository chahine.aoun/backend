package bankingapp.backend.registration;


import bankingapp.backend.files.FileEntity;
import lombok.*;

import javax.validation.constraints.*;



@Getter
@EqualsAndHashCode
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class RegistrationRequest {
    @Pattern(regexp = "[A-Za-z\\ '\"'%\\-]+",
            message = "Make sure you entered your first name correctly.")
    @NotBlank(message = "First name is required.")
    @Size(min = 3, max = 50, message = "Name must be between 3 and 50 characters")
    private String firstName;

    @Pattern(regexp = "[A-Za-z\\ '\"'%\\-]+",
            message = "Make sure you entered your last name correctly.")
    @NotBlank(message = "Last name is required.")
    @Size(min = 3, max = 50, message = "Last name must be between 3 and 50 characters")
    private String lastName;

    @Email(message = "Make sure you typed your email address correctly.")
    private String email;

    @NotBlank(message = "Phone number is required.")
    private String phoneNumber;

    @NotBlank(message = "Password is required.")
    @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$",
            message = "Password must be longer than 8 characters and contain at least" +
                    " one upper case letter,one number and one special character.")
    private String password;

    @NotNull(message = "Photo  is required.")
    private FileEntity file;

    @Pattern(regexp = "(ID card)|(Passport)",
            message = "Make sure you entered your ID type correctly.")
    @NotBlank(message = "Please specify you id type.")
    private String typeOfID;

    @Pattern(regexp = "[0-9\\ \"'%\\-]+",
            message = "Make sure you entered your ID's number correctly.")
    @NotBlank(message = "The id number is required.")
    private String idNumber;

    @Pattern(regexp = "[0-9\\ \"'%\\-]+",
            message = "Make sure you entered your referral code correctly.")
    private String referralCode;

    @NotBlank(message = "Please specify your ID's expiration date.")
    @Pattern(regexp = "^\\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$",
           message = "make sure you entered the expiration date correctly.")
    private String expirationDate;

    @NotBlank(message = "Please specify your birthdate.")
    @Pattern(regexp = "^\\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$",
          message = "Make sure you entered your birth date correctly.")
    private String birthday;

    @Pattern(regexp = "(male)|(female)",
            message = "Make sure you entered your gender correctly.")
    @NotBlank(message = "Please specify your gender.")
    private String gender;

    @Pattern(regexp = "[A-Za-z\\ \"'%\\-]+",
            message = "Make sure you entered your country of residence correctly.")
    @NotBlank(message = "Please specify your current country of residence.")
    private String countryOfResidence;

    @Pattern(regexp = "[A-Za-z0-9\\.\\s,\\-:\\n]+",
            message = "Make sure you entered your address correctly.")
    @NotBlank(message = "Please specify your address.")
    private String address;


    public void setFile(FileEntity file){
        this.file=file;
    }

}
