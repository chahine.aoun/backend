package bankingapp.backend.registration;


import bankingapp.backend.email.EmailSender;
import bankingapp.backend.email.EmailService;
import bankingapp.backend.registration.token.ConfirmationToken;
import bankingapp.backend.registration.token.ConfirmationTokenService;
import bankingapp.backend.user.Role;
import bankingapp.backend.user.UserEntity;
import bankingapp.backend.user.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;


@Service
@AllArgsConstructor
public class RegistrationService {

    private final UserService userService;
    private final ConfirmationTokenService confirmationTokenService;
    private final EmailSender emailSender;
    private final EmailService emailservice;

    public String register(@Valid RegistrationRequest request) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<RegistrationRequest>> violations = validator.validate(request);
        if (violations.isEmpty()) {
            List<String> token = userService.signUpUser(
                new UserEntity(
                        request.getFirstName(),
                        request.getLastName(),
                        request.getEmail(),
                        request.getPhoneNumber(),
                        request.getPassword(),
                        Role.USER,
                        request.getFile(),
                        request.getTypeOfID(),
                        request.getIdNumber(),
                        request.getBirthday(),
                        request.getCountryOfResidence(),
                        request.getAddress(),
                        request.getReferralCode(),
                        request.getExpirationDate(),
                        request.getGender(),"0"
                )
        );
        String link = "http://localhost:8081/app/reg/confirm?token=" + token.get(0);
        emailSender.send(
                request.getEmail(),
                emailservice.buildEmail(request.getFirstName(), link,token.get(1)));
        return "Account created successfully.";
        } else {
            return violations.toString();
        }
    }
    public void registerAdmin(RegistrationRequest request) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<RegistrationRequest>> violations = validator.validate(request);
        if (violations.isEmpty()) {
        String token = userService.signUpUser(
                new UserEntity(
                        request.getFirstName(),
                        request.getLastName(),
                        request.getEmail(),
                        request.getPhoneNumber(),
                        request.getPassword(),
                        Role.ADMIN,
                        request.getFile(),
                        request.getTypeOfID(),
                        request.getIdNumber(),
                        request.getBirthday(),
                        request.getCountryOfResidence(),
                        request.getAddress(),
                        request.getReferralCode(),
                        request.getExpirationDate(),
                        request.getGender(),"0"
                )
        ).get(0);
        String link = "http://localhost:8081/app/reg/confirm?token=" + token;
        emailSender.send(
                request.getEmail(),
                emailservice.buildEmail(request.getFirstName(), link));
        } else {
            violations.forEach(violation -> {
                String message =violation.getMessage();
                System.out.println(message);
            });
        }
    }

    @Transactional
    public String confirmToken(String token) {
        ConfirmationToken confirmationToken = confirmationTokenService
                .getToken(token)
                .orElseThrow(() ->
                        new IllegalStateException("token not found"));

        if (confirmationToken.getConfirmedAt() != null) {
            throw new IllegalStateException("email already confirmed");
        }

        LocalDateTime expiredAt = confirmationToken.getExpiresAt();

        if (expiredAt.isBefore(LocalDateTime.now())) {
            throw new IllegalStateException("token expired");
        }

        confirmationTokenService.setConfirmedAt(token);
        userService.enableUser(
                confirmationToken.getAccount());
        return "confirmed";
    }

    public boolean emailTest(String email){
        return userService.emailTest(email);
    }
    public boolean phoneTest(String phone){
        return userService.phoneTest(phone);
    }
    public boolean registrationValidator(RegistrationRequest request){
        boolean z=true;
        return z;
    }

    public String getUserNumber(String email) {
       return  userService.getUser(email).getPhoneNumber();
    }
}
