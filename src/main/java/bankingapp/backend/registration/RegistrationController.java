package bankingapp.backend.registration;

import bankingapp.backend.files.FileService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping(path = "/app/reg")
@AllArgsConstructor
public class RegistrationController {

    private final RegistrationService registrationService;
    private final FileService fileService;


    @GetMapping(path = "/getNumber")
    public void getNumber(@RequestParam("email")@NonNull String email,HttpServletResponse response) {
        response.setHeader("number",registrationService.getUserNumber(email));
    }

    @GetMapping(path = "/test/email/{email}")
    public void testEmail(@PathVariable("email")@NonNull String email, HttpServletResponse response) {
        response.setHeader("taken",""+registrationService.emailTest(email));
    }
    @GetMapping(path = "/test/phone/{phone}")
    public void testPhone(@PathVariable("phone")@NonNull String phone, HttpServletResponse response) {
        response.setHeader("taken",""+registrationService.phoneTest(phone));
    }

    @PostMapping
    public String register(@RequestParam("user")@NonNull String json,
                                           @RequestParam("file")@NonNull MultipartFile file) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode node = mapper.readTree(json);
            RegistrationRequest request = mapper.treeToValue(node, RegistrationRequest.class);
                request.setFile(fileService.save(file));
                return registrationService.register(request);
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @PostMapping(path="/admin")
    public ResponseEntity<String> registerAdmin(@RequestParam("user") @NonNull String json,
                                                @RequestParam("file")@NonNull MultipartFile file) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode node = mapper.readTree(json);
            RegistrationRequest request = mapper.treeToValue(node, RegistrationRequest.class);
            request.setFile(fileService.save(file));
            registrationService.registerAdmin(request);
            return ResponseEntity.status(HttpStatus.OK)
                    .body("Registration success ");

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }
    @GetMapping(path = "/confirm")
    public String confirm(@RequestParam("token")@NonNull String token) {
        return registrationService.confirmToken(token);
    }
}

