package bankingapp.backend.files;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Service
    @RequiredArgsConstructor
    public class FileService {
        private final FileRepo fileRepository;


        public FileEntity save(MultipartFile file) throws IOException {
            if(!file.isEmpty()){
            FileEntity fileEntity = new FileEntity();
            fileEntity.setName(StringUtils.cleanPath(file.getOriginalFilename()));
            fileEntity.setContentType(file.getContentType());
            fileEntity.setData(file.getBytes());
            fileEntity.setSize(file.getSize());
           //TODO work on file types and ....
          //  if (file.getOriginalFilename().contains(".PNG")|| file.getOriginalFilename().contains(".jpg")){
            fileRepository.save(fileEntity);
            return fileEntity;
         //   } else {
          //      throw new IllegalArgumentException("File type mismatch");
         //   }

           } else {
                throw new IllegalArgumentException("You need to upload a file");
         }
        }


        public FileResponse entityToResponse(FileEntity fileEntity){
            String downloadURL = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/app/account/files/")
                    .path(fileEntity.getId())
                    .toUriString();
            FileResponse fileResponse = new FileResponse();
            fileResponse.setId(fileEntity.getId());
            fileResponse.setName(fileEntity.getName());
            fileResponse.setContentType(fileEntity.getContentType());
            fileResponse.setSize(fileEntity.getSize());
            fileResponse.setUrl(downloadURL);
            return fileResponse;
        }


        public Optional<FileEntity> getFile(String id) {

            return fileRepository.findById(id);
        }


        public List<FileEntity> getAllFiles() {

            return fileRepository.findAll();
        }

        public void deleteFile(String id){
            fileRepository.deleteById(id);
        }
}
