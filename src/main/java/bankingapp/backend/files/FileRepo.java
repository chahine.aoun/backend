package bankingapp.backend.files;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepo extends JpaRepository<FileEntity, String> {

    @Transactional
    @Modifying
    @Query("UPDATE FileEntity a " +
    "SET a.data = ?2 , a.size = ?3 , a.contentType = ?4 WHERE a.id = ?1")
int updateFile(String  id,byte[] data,Long size,String contentType);
}